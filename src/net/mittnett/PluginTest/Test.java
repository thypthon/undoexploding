package net.mittnett.PluginTest;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class Test extends JavaPlugin {

	public static final Logger log = Logger.getLogger("Minecraft");
	
	@Override
	public void onDisable(){
		Bukkit.getServer().getScheduler().cancelTasks(this);
		log.log(Level.INFO, "Plugin avsluttet.");
	}
	
	@Override
	public void onEnable(){
		log.log(Level.INFO, "Plugin laster inn...");
		getServer().getPluginManager().registerEvents(new EntityListener(this), this);
		log.log(Level.INFO, "Plugin lastet inn.");
	}
	
}
