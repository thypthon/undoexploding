package net.mittnett.PluginTest;

import java.util.HashSet;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;

public class UndoExploding implements Runnable {

	@SuppressWarnings("unused")
	private Test plugin;
	private HashSet<SpessBlokk> blokkene;

	public UndoExploding(Test plugin, HashSet<SpessBlokk> blocks) {
		this.plugin = plugin;
		this.blokkene = blocks;
	}
	
	@Override
	public void run() {
		for(SpessBlokk b : blokkene){
			Location loc = b.getLoc();
			World wo = Bukkit.getWorld(loc.getWorld().getName());
			if(wo != null){
				Block atm = wo.getBlockAt(b.getLoc());
				atm.setTypeId(b.getTheType());
			}
		}
		
		blokkene.clear();
	}

}
