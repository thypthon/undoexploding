package net.mittnett.PluginTest;

import org.bukkit.Location;

public class SpessBlokk {

	private int theType;
	private Location loc;
	
	public SpessBlokk(int type, Location l){
		this.theType = type;
		this.loc = l;
	}
	
	public int getTheType() {
		return theType;
	}

	public void setTheType(int theType) {
		this.theType = theType;
	}

	public Location getLoc() {
		return loc;
	}
	
	public void setLoc(Location loc) {
		this.loc = loc;
	}
	
}
