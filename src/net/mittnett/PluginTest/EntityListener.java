package net.mittnett.PluginTest;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;

public class EntityListener implements Listener {

	private Test plugin;
	
	public EntityListener(Test instance){
		this.plugin = instance;
	}
	
	@EventHandler
	public void onEntityExplode(EntityExplodeEvent e){
		//Entity entity = e.getEntity();
		HashSet<SpessBlokk> blokkene = new HashSet<SpessBlokk>();
		List<Block> destroyed = e.blockList();
        Iterator<Block> it = destroyed.iterator();
        while (it.hasNext()) {
            Block block = it.next();
            blokkene.add(new SpessBlokk(block.getTypeId(), block.getLocation()));
        }
        
        Bukkit.getServer().getScheduler().runTaskLaterAsynchronously(plugin, new UndoExploding(plugin, blokkene), 100L);
	}
	
}
